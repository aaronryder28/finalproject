// Proof of concept Face Ai detection  
// ml5.js p5.js faceApi aryde002
// WAIT FOR MODEL LOADED IS READY ...   

let faceapi; // variable to store face api
let video; // variable to store video input
let detections = []; //variable to store results in array 

// these are our options for detecting faces, provided by ml5.js
// to use Landmarks set boolean to true
// not using descriptors set boolean to false 
// min thresh hold of confidence to 0.2 threshold between 1 to 0 
const detection_options = {
    withLandmarks: true,  
    withDescriptors: false,
    minConfidence: 0.2
    
}

function setup() {
  createCanvas(720, 480); // 720p stream video
 
  // Webcam access using webcamReady callback when access granted
  video = createCapture(VIDEO, webcamReady);
  video.size(width, height); // set size equal to canvas
  video.hide(); // hide element dom
}

function webcamReady(stream) {
  // loading the faceapi model while modelReady callback
  faceapi = ml5.faceApi(video, detection_options, modelReady)
}

function draw() {
  background(0);
  
  // draw picture
  image(video, 0,0, width, height)
  
  // if detection draw on the image
  if (detections) {
    // call detect used to detect more than one 
    // face by using a for loop to get each person
    // ml5 returns an array of objects
    for (let iface of detections) {
      drawBox(iface);
      drawLandmarks(iface);
      
      
    }
  }
}


// when the model is read so when ml5 has loaded the model use callback 
function modelReady() {
  console.log("Model loaded is ready...");

  // ml5 detect faces in the video stream if so gotResults callback
  faceapi.detect(gotResults);
}

// ml5 decides if there are faces 
function gotResults(err, result) {
    //  error handling if error print to console and stop
    if (err) {
        console.log(err)
        return
    }
      
    // store results in the detections variable
    // object of detections check console for results
    console.log(result);
    detections = result;
    
    // call face detect for results 
    faceapi.detect(gotResults)
}
  

// Draw face elements   
function drawBox(detections){
    const alignedRect = detections.alignedRect;
    const {_x, _y, _width, _height} = alignedRect._box;
    fill(0,0,0);
    stroke(161, 95, 251);
    strokeWeight(2)
    rect(_x, _y, _width, _height)
}

  
// forEach used to loop through objects in an array returning 
// each value in the array in sequence as item local variable 
function drawLandmarks(detections){
  
    noFill();
    // randomise colors for face effect
    stroke(random(20, 245), random (20, 245), random(20, 245));
    strokeWeight(2)
    
  // push pop used to only apply to these values 
    push()
  
    // jawOutline
    beginShape();
    detections.parts.jawOutline.forEach(item => {
        vertex(item._x, item._y)
      
    })
    endShape(CLOSE);
  
    // mouth
    beginShape();
    detections.parts.mouth.forEach(item => {
        vertex(item._x, item._y)
    })
    endShape(CLOSE);

    // nose
    beginShape();
    detections.parts.nose.forEach(item => {
        vertex(item._x, item._y)
       
    })
    endShape(CLOSE);

   // right eye
    beginShape();
    detections.parts.rightEye.forEach(item => {
        vertex(item._x, item._y)
    })
    endShape(CLOSE);
  
  // left eye
    beginShape();
    detections.parts.leftEye.forEach(item => {
        vertex(item._x, item._y)
    })
    endShape(CLOSE);

    // right eyebrow
    beginShape();
    detections.parts.rightEyeBrow.forEach(item => {
        vertex(item._x, item._y)
    })
    endShape();

    // left eye
    beginShape();
    detections.parts.leftEyeBrow.forEach(item => {
        vertex(item._x, item._y)
    })
    endShape();

    pop();
    
}
  
  