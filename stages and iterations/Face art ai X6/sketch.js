/*
 Final project version X6
  
*/

let video,
    poseNet,
    pose,
    skeleton,
    button,
    iCross = 180,
    lCross = 180,
    jCross = 180,
    kCross = 180,
    xNose = 0,
    yNose = 0,
    xLeye = 0,   
    yLeye = 0, 
    img,
    scene;


function preload() {
  
  // star image png 
  img = loadImage('assets/astar.png');
  
  
}



function setup() {
  createCanvas(640, 480);

  // load camera
  video = createCapture(VIDEO, cameraLoaded);
  video.size(640, 480);
  video.hide(); // hide the dom element
  angleMode(DEGREES); // set angles to degrees 
  scene = 1;
 
  // button link to website 
  button = createButton('Save');
  button.position(300, 485);
  button.mousePressed(saveImagexyz);
  
}

function saveImagexyz(){
  
  save("myImage.png");
  
}

function cameraLoaded(stream) {
  // load posenet
  poseNet = ml5.poseNet(video, modelLoaded);
  poseNet.on("pose", gotPoses); // setup callback for pose detection

}


// this is called when we can detect a pose
function gotPoses(poses) {
  if (poses.length > 0) {
    pose = poses[0].pose;
    skeleton = poses[0].skeleton;
    
    //this is calculate distance from camera 
    let nosX = poses[0].pose.keypoints[0].position.x;
    let nosY = poses[0].pose.keypoints[0].position.y;
    let eyX = poses[0].pose.keypoints[1].position.x;
    let eyY = poses[0].pose.keypoints[1].position.y;
    
    xNose = lerp(xNose, nosX, 0.5);
    xNose = lerp(yNose, nosY, 0.5);
    xLeye = lerp(xLeye, eyX, 0.5);
    yLeye = lerp(yLeye, eyY, 0.5);
    
    
  }
}

function draw() {
  translate(video.width, 0);
  scale(-1, 1); // this flips the video so it's easier for us
  image(video, 0, 0, video.width, video.height);
  // distance from webcam 
  let d = dist(xNose, yNose, xLeye, yLeye);
  
  // if we can detect a pose, let's draw it
  if (pose) {
    
    // skeleton 
    for (let bone of skeleton) {
      let a = bone[0];
      let b = bone[1];
      strokeWeight(2);
      stroke(0);
      line(a.position.x, a.position.y, b.position.x, b.position.y);
    }
    
    
   
    // pose dots 
    /*
    for (let keypoint of pose.keypoints) {
      let x = keypoint.position.x;
      let y = keypoint.position.y;
      fill(0);
      stroke(255);
      ellipse(x, y, 16, 16);
    }
    */
    
 // xxxxxxxxxxxxxxxxxxxxxxxxxx SCENE ONE xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    
    if (scene == 1) {
      
      push();
      fill(200);
      rect(0,0,640,480);
      fill(0,0,0);
      textSize(60)
      text('IA TЯA ƎƆAᖷ',150,80);
      textSize(30);
      text('Ɉƨil ИoiɈɔυɿɈƨИI',220,120);
      textSize(25)
      text(' TЯA ƎƆAᖷ ƧƧƎƆƆA OT ЯƎꓷЯO ЯƎꓭMUИ ИI ट OT ᛚ ƧƧƎЯᕋ',40,160);
      
      
      pop();
      
      
      
         // next scene    
      
      
    if(keyIsPressed){
    if (key == 1) {
      //do something
      scene = 2;
      
      } 
     } 
      
      
    }
    
    
    
    
    
    
    
    
 // xxxxxxxxxxxxxxxxxxxxxxxxxx SCENE TWO xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    
    
    if (scene == 2) {
    
      // scene tracking number
    push();  
    fill(0);
    textSize(40);
    text("ᛚ", 40, 40);
    pop();  
      
      
      
      
      
      
      
    
    // face art 
    
    fill(210);
    // robot neck
    rect(pose.nose.x - 13 , pose.nose.y - 80, 30, 200);
    // robot head
    rect(pose.nose.x - 73 , pose.nose.y - 120, 160, 200);
    // robot eye scanning animation 
    fill(0);
    rect(pose.nose.x - 43 , pose.nose.y - 70, 100, 40);
    fill(random(10,255),random(10,255),random(10,255));
    rect(pose.nose.x - random(-40,40) , pose.nose.y - 60, 15, 15);
    push();
    fill(255);
    rect(pose.nose.x - 93 , pose.nose.y - 140, 70, 210);
    rect(pose.nose.x - 93 , pose.nose.y + 10, 195, 50);
    rotate(15);
    fill(255,0,0);
    rect(pose.nose.x + 76 , pose.nose.y - 210, 80, 200);
    pop(); 
  
    
    // animation 
    
    
    
    
    
    
    
    //guidecross
    
   push();
   if (iCross > 0){
    iCross = iCross - 0.6;
   }
   stroke(220, 220, 220, iCross);
   strokeWeight(5);
   line(320, 0, 320, 480);
   line(0, 240, 680, 240);
   pop();
    
    
   // next scene    
      
      
    if(keyIsPressed){
    if (key == 2) {
      //do something
      scene = 3;
      
      } 
     } 
    }  
      
      
 // xxxxxxxxxxxxxxxxxxxxxxxxxx SCENE THREE xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 
      
      
      
    
    
    if (scene == 3) {
    // draw scene 3
    // scene tracking number
    push();  
    fill(0);
    textSize(40);
    text("𐑕", 40, 40);
    pop();  
      
      
      
  // face art 
      push();
      image(img, pose.leftEye.x -12, pose.leftEye.y -12, 20, 20);
      image(img, pose.rightEye.x - 12, pose.rightEye.y -12, 20, 20);
      noFill();
      
      fill(255,255,0);
      stroke(0,0,0);
      strokeWeight(5);
      // head 
      rect(pose.nose.x - 0, pose.nose.y - 60 ,10, 60);
      
      fill(255,255,0);
      stroke(0,0,0);
      strokeWeight(20);
    
      // mask
      rect(pose.nose.x - 80, pose.nose.y - 120 ,170, 60);
      fill(0,random(200,150));
      rect(pose.nose.x - 90, pose.nose.y - 60 ,190, 60);
      fill(random(50,240),random(50,240),random(50, 240));
      rect(pose.nose.x - 100, pose.nose.y + 30 ,220, 80);
      triangle(pose.nose.x + 120 , pose.nose.y ,pose.nose.x, pose.nose.y + 120 , pose.nose.x - 90, pose.nose.y);
      pop();
  
    
    // animation 
      
      
      
        //guidecross
  
   push();
   if (lCross > 0){
    lCross = lCross -0.6;
   }
   stroke(220, 220, 220, lCross);
   strokeWeight(5);
   line(320, 0, 320, 480);
   line(0, 240, 680, 240);
   pop();
      
      
      
      
      
      
      
      
      
      
        // next scene    
      
        if(keyIsPressed){
    if (key == 3) {
      //do something
      scene = 4;
      
    } 
    } 
  
      
      
      
  }
    
    
    
 // xxxxxxxxxxxxxxxxxxxxxxxxxx SCENE FOUR xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    
    if (scene == 4) {
      
      
         // scene tracking number
    push();  
    fill(0);
    textSize(40);
    text("Ԑ", 40, 40);
    pop();  
      
      
      
      
      
    
    // face art 
    
    fill(250,0,0);
   
    // robot head
    rect(pose.nose.x - 89 , pose.nose.y - 120, 180, 240);
    
    fill(0);
    
    
    // animation 
    
    
    
    
    
    
    
    //guidecross
  
   push();
   if (jCross > 0){
    jCross = jCross -0.6;
   }
   stroke(220, 220, 220, jCross);
   strokeWeight(5);
   line(320, 0, 320, 480);
   line(0, 240, 680, 240);
   pop();
      
      
      
            // next scene    
      
        if(keyIsPressed){
    if (key == 4) {
      //do something
      scene = 5;
      
    } 
    } 
      
      
      
      
      
      
      
      
    
    
  }
    
    
    
    
 // xxxxxxxxxxxxxxxxxxxxxxxxx SCENE FIVE xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    
    
     if (scene == 5) {
      
      
    // scene tracking number
    push();  
    fill(0);
    textSize(40);
    text("𐊀", 40, 40);
    pop();  
      
    
    // face art 
     fill(250,150,0);
   
    // robot head
    rect(pose.nose.x - 89 , pose.nose.y - 120, 180, 240);
    
    fill(0);
    
    // animation 
    
    
    
    //guidecross
  
   push();
   if (kCross > 0){
    kCross = kCross -0.6;
   }
   stroke(220, 220, 220, kCross);
   strokeWeight(5);
   line(320, 0, 320, 480);
   line(0, 240, 680, 240);
   pop();
    
       
    // next scene    
      
    if(keyIsPressed){
    if (key == 5) {
      //do something
      scene = 6;
      
    } 
    } 
           
    
  }
    
    
    
 // xxxxxxxxxxxxxxxxxxxxxxxxx SCENE SIX xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    
    
     if (scene == 6) {
      
      
      
      
    
    // face art 
     push();
      fill(200);
      rect(0,0,640,480);
      fill(0,0,0);
      textSize(60)
      text('ꓷИƎ',250,80);
      text('ɈɿɒɈƨǝɿ oɈ მ ƨƨǝɿq',80,180);
      pop();
    
       
       // scene tracking number
    push();  
    fill(0);
    textSize(40);
    text("ट", 40, 40);
    pop();
       
       
       
       
       
       
       
       
       
    // animation 
    
    
       
    // next scene    
      
        if(keyIsPressed){
    if (key == 6) {
      //do something
     scene = 1;
      
    } 
    } 
           
    
  }
       

    
    
  }
  
  
 
}



function modelLoaded() {
  console.log("poseNet ready");
}
