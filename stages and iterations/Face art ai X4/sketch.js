/*
 Final project version X4
  
*/

let video,
    poseNet,
    pose,
    skeleton,
    button,
    i = 180,
    l = 180,
    j = 180,
    k = 180,
    z = 180,
    a = 180,
    img,
    scene;


function preload() {
  
  // star image png 
  img = loadImage('astar.png');
  
  
}



function setup() {
  createCanvas(640, 480);

  // load camera
  video = createCapture(VIDEO, cameraLoaded);
  video.size(640, 480);
  video.hide(); // hide the dom element
  angleMode(DEGREES); // set angles to degrees 
  scene = 1;
 
  // button link to website 
  button = createButton('Save');
  button.position(300, 485);
  button.mousePressed(saveImagexyz);
  
}

function saveImagexyz(){
  
  
  
}

function cameraLoaded(stream) {
  // load posenet
  poseNet = ml5.poseNet(video, modelLoaded);
  poseNet.on("pose", gotPoses); // setup callback for pose detection

}


// this is called when we can detect a pose
function gotPoses(poses) {
  if (poses.length > 0) {
    pose = poses[0].pose;
    skeleton = poses[0].skeleton;
    
  }
}

function draw() {
  translate(video.width, 0);
  scale(-1, 1); // this flips the video so it's easier for us
  image(video, 0, 0, video.width, video.height);

  
  // if we can detect a pose, let's draw it
  if (pose) {
    
    // skeleton 
    for (let bone of skeleton) {
      let a = bone[0];
      let b = bone[1];
      strokeWeight(2);
      stroke(0);
      line(a.position.x, a.position.y, b.position.x, b.position.y);
    }
   
    // pose dots 
    /*
    for (let keypoint of pose.keypoints) {
      let x = keypoint.position.x;
      let y = keypoint.position.y;
      fill(0);
      stroke(255);
      ellipse(x, y, 16, 16);
    }
    */
    
 // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    
    if (scene == 1) {
      
      push();
      fill(200);
      rect(0,0,640,480);
      fill(0,0,0);
      textSize(60)
      text('IA TЯA ƎƆAᖷ',150,80);
      textSize(30)
      text('Ɉƨil ИoiɈɔυɿɈƨИI',220,120);
      pop();
      
      
      
         // next scene    
      
      
    if(keyIsPressed){
    if (key == 1) {
      //do something
      scene = 2;
      
    } 
    } 
      
      
    }
    
    
    
    
    
    
    
    
 // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    
    
    if (scene == 2) {
    
      // scene tracking number
    push();  
    fill(0);
    textSize(40);
    text("ᛚ", 40, 40);
    pop();  
      
      
      
      
      
      
      
    
    // face art 
    
    fill(210);
    // robot neck
    rect(pose.nose.x - 13 , pose.nose.y - 80, 30, 200);
    // robot head
    rect(pose.nose.x - 73 , pose.nose.y - 120, 160, 200);
    // robot eye scanning animation 
    fill(0);
    rect(pose.nose.x - 43 , pose.nose.y - 70, 100, 40);
    fill(random(10,255),random(10,255),random(10,255));
    rect(pose.nose.x - random(-40,40) , pose.nose.y - 60, 15, 15);
    push();
    fill(255);
    rect(pose.nose.x - 93 , pose.nose.y - 140, 70, 210);
    rect(pose.nose.x - 93 , pose.nose.y + 10, 195, 50);
    rotate(15);
    fill(255,0,0);
    rect(pose.nose.x + 76 , pose.nose.y - 210, 80, 200);
    pop(); 
  
    
    // animation 
    
    
    
    
    
    
    
    //guidecross
    
   push();
   if (i > 0){
    i = i - 0.5;
   }
   stroke(220, 220, 220, i);
   strokeWeight(5);
   line(320, 0, 320, 480);
   line(0, 240, 680, 240);
   pop();
    
    
   // next scene    
      
      
    if(keyIsPressed){
    if (key == 2) {
      //do something
      scene = 3;
      
    } 
    } 
      
      
      
 // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 
      
      
      
    }
    
    if (scene == 3) {
    // draw scene 3
    // scene tracking number
    push();  
    fill(0);
    textSize(40);
    text("𐑕", 40, 40);
    pop();  
      
      
      
  // face art 
      push();
    image(img, pose.leftEye.x -12, pose.leftEye.y -12, 20, 20);
      image(img, pose.rightEye.x - 12, pose.rightEye.y -12, 20, 20);
      noFill();
      
      fill(255,255,0);
      stroke(0,0,0);
      strokeWeight(5);
      // head 
      rect(pose.nose.x - 0, pose.nose.y - 60 ,10, 60);
      
      fill(255,255,0);
      stroke(0,0,0);
      strokeWeight(20);
    
      // mask
      rect(pose.nose.x - 80, pose.nose.y - 120 ,170, 60);
      fill(0,random(200,150));
      rect(pose.nose.x - 90, pose.nose.y - 60 ,190, 60);
      fill(random(50,240),random(50,240),random(50, 240));
      rect(pose.nose.x - 100, pose.nose.y + 30 ,220, 80);
      triangle(pose.nose.x + 120 , pose.nose.y ,pose.nose.x, pose.nose.y + 120 , pose.nose.x - 90, pose.nose.y);
      pop();
  
    
    // animation 
      
      
      
        //guidecross
  
   push();
   if (l > 0){
    l = l -0.5;
   }
   stroke(220, 220, 220, l);
   strokeWeight(5);
   line(320, 0, 320, 480);
   line(0, 240, 680, 240);
   pop();
      
      
      
      
      
      
      
      
      
      
        // next scene    
      
        if(keyIsPressed){
    if (key == 3) {
      //do something
      scene = 4;
      
    } 
    } 
  
      
      
      
  }
    
    
    
 // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    
    if (scene == 4) {
      
      
         // scene tracking number
    push();  
    fill(0);
    textSize(40);
    text("Ԑ", 40, 40);
    pop();  
      
      
      
      
      
    
    // face art 
    
    fill(250,0,0);
   
    // robot head
    rect(pose.nose.x - 89 , pose.nose.y - 120, 180, 240);
    
    fill(0);
    
    
    // animation 
    
    
    
    
    
    
    
    //guidecross
  
   push();
   if (j > 0){
    j = j -0.5;
   }
   stroke(220, 220, 220, j);
   strokeWeight(5);
   line(320, 0, 320, 480);
   line(0, 240, 680, 240);
   pop();
      
      
      
            // next scene    
      
        if(keyIsPressed){
    if (key == 4) {
      //do something
      scene = 5;
      
    } 
    } 
      
      
      
      
      
      
      
      
    
    
  }
    
    
    
    
 // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    
    
     if (scene == 5) {
      
      
    // scene tracking number
    push();  
    fill(0);
    textSize(40);
    text("𐊀", 40, 40);
    pop();  
      
    
    // face art 
     fill(250,150,0);
   
    // robot head
    rect(pose.nose.x - 89 , pose.nose.y - 120, 180, 240);
    
    fill(0);
    
    // animation 
    
    
    
    //guidecross
  
   push();
   if (k > 0){
    k = k -0.5;
   }
   stroke(220, 220, 220, k);
   strokeWeight(5);
   line(320, 0, 320, 480);
   line(0, 240, 680, 240);
   pop();
    
       
    // next scene    
      
    if(keyIsPressed){
    if (key == 5) {
      //do something
      scene = 6;
      
    } 
    } 
           
    
  }
    
    
    
 // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    
    
    if (scene == 6) {
      
      
    // scene tracking number
    push();  
    fill(0);
    textSize(40);
    text("ट", 40, 40);
    pop();  
      
    
    // face art 
     fill(0,250,0);
   
    // robot head
    rect(pose.nose.x - 89 , pose.nose.y - 120, 180, 240);
    
    fill(0);
    
    // animation 
    
    
    
    //guidecross
  
   push();
   if (z > 0){
    z = z -0.5;
   }
   stroke(220, 220, 220, z);
   strokeWeight(5);
   line(320, 0, 320, 480);
   line(0, 240, 680, 240);
   pop();
    
       
    // next scene    
      
        if(keyIsPressed){
    if (key == 6) {
      //do something
      scene = 7;
      
    } 
    } 
           
    
  }
    
    
    
 // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    
    
     if (scene == 7) {
      
      
    // scene tracking number
    push();  
    fill(0);
    textSize(40);
    text("მ", 40, 40);
    pop();  
      
    
    // face art 
     fill(0,0,250);
   
    // robot head
    rect(pose.nose.x - 89 , pose.nose.y - 120, 180, 240);
    
    fill(0);
    
    // animation 
    
    
    
    //guidecross
  
   push();
   if (a > 0){
    a = a -0.5;
   }
   stroke(220, 220, 220, a);
   strokeWeight(5);
   line(320, 0, 320, 480);
   line(0, 240, 680, 240);
   pop();
    
       
    // next scene    
      
        if(keyIsPressed){
    if (key == 7) {
      //do something
      scene = 8;
      
    } 
    } 
           
    
  }
    
    
    
    
 // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    
    
     if (scene == 8) {
      
      
    // scene tracking number
    push();  
    fill(0);
    textSize(40);
    text("𐊀", 40, 40);
    pop();  
      
    
    // face art 
     push();
      fill(200);
      rect(0,0,640,480);
      fill(0,0,0);
      textSize(60)
      text('ꓷИƎ',250,80);
      text('ɈɿɒɈƨǝɿ oɈ 8 ƨƨǝɿq',80,180);
      pop();
    
    // animation 
    
    
       
    // next scene    
      
        if(keyIsPressed){
    if (key == 8) {
      //do something
     scene = 1;
      
    } 
    } 
           
    
  }
       

    
    
  }
  
  
 
}



function modelLoaded() {
  console.log("poseNet ready");
}
