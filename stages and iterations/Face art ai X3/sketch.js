/*
 Final project version X3 v1
  
*/

let video,
    poseNet,
    pose,
    skeleton,
    button,
    i = 180,
    l = 180,
    j,
    k,
    z,
    a,
    scene;



function setup() {
  createCanvas(640, 480);

  // load camera
  video = createCapture(VIDEO, cameraLoaded);
  video.size(640, 480);
  video.hide(); // hide the dom element
  angleMode(DEGREES); // set angles to degrees 
  scene = 1;
  
}

function cameraLoaded(stream) {
  // load posenet
  poseNet = ml5.poseNet(video, modelLoaded);
  poseNet.on("pose", gotPoses); // setup callback for pose detection

}


// this is called when we can detect a pose
function gotPoses(poses) {
  if (poses.length > 0) {
    pose = poses[0].pose;
    skeleton = poses[0].skeleton;
    
  }
}

function draw() {
  translate(video.width, 0);
  scale(-1, 1); // this flips the video so it's easier for us
  image(video, 0, 0, video.width, video.height);

  
  // if we can detect a pose, let's draw it
  if (pose) {
    
    // skeleton 
    for (let bone of skeleton) {
      let a = bone[0];
      let b = bone[1];
      strokeWeight(2);
      stroke(0);
      line(a.position.x, a.position.y, b.position.x, b.position.y);
    }
   
    // pose dots 
    /*
    for (let keypoint of pose.keypoints) {
      let x = keypoint.position.x;
      let y = keypoint.position.y;
      fill(0);
      stroke(255);
      ellipse(x, y, 16, 16);
    }
    */
    
    if (scene == 1) {
      
      push();
      fill(200);
      rect(0,0,640,480);
      fill(0,0,0);
      textSize(60)
      text('IA TЯA ƎƆAᖷ',150,80);
      textSize(30)
      text('Ɉƨil ИoiɈɔυɿɈƨИI',220,120);
      pop();
      
      
      
         // next scene    
      
      
    if(keyIsPressed){
    if (key == 1) {
      //do something
      scene = 2;
      
    } 
    } 
      
      
    }
    
    
    
    
    
    
    
    
    
    
    
    if (scene == 2) {
    
    
    // face art 
    
    fill(210);
    // robot neck
    rect(pose.nose.x - 13 , pose.nose.y - 80, 30, 200);
    // robot head
    rect(pose.nose.x - 73 , pose.nose.y - 120, 160, 200);
    // robot eye scanning animation 
    fill(0);
    rect(pose.nose.x - 43 , pose.nose.y - 70, 100, 40);
    fill(random(10,255),random(10,255),random(10,255));
    rect(pose.nose.x - random(-40,40) , pose.nose.y - 60, 15, 15);
    push();
    fill(255);
    rect(pose.nose.x - 93 , pose.nose.y - 140, 70, 210);
    rect(pose.nose.x - 93 , pose.nose.y + 10, 195, 50);
    rotate(15);
    fill(255,0,0);
    rect(pose.nose.x + 76 , pose.nose.y - 210, 80, 200);
    pop(); 
  
    
    // animation 
    
    
    
    
    
    
    
    //guidecross
    
   push();
   if (i > 0){
    i = i -0.5;
   }
   stroke(220, 220, 220, i);
   strokeWeight(5);
   line(320, 0, 320, 480);
   line(0, 240, 680, 240);
   pop();
    
    
   // next scene    
      
      
    if(keyIsPressed){
    if (key == 2) {
      //do something
      scene = 3;
      
    } 
    } 
      
      
      
      
      
      
      
    }
    
    if (scene == 3) {
    // draw scene 2
    fill(random(255), random(255), random(255));
    text("CANDY!!!", 150, 200);
      
        // next scene    
      
        if(keyIsPressed){
    if (key == 3) {
      //do something
      scene = 4;
      
    } 
    } 
  
      
      
      
  }
    
    if (scene == 4) {
    
    // face art 
    
    fill(210);
    // robot neck
    rect(pose.nose.x - 13 , pose.nose.y - 80, 30, 200);
    // robot head
    rect(pose.nose.x - 73 , pose.nose.y - 120, 160, 200);
    // robot eye scanning animation 
    fill(0);
    rect(pose.nose.x - 43 , pose.nose.y - 70, 100, 40);
    fill(random(10,255),random(10,255),random(10,255));
    rect(pose.nose.x - random(-40,40) , pose.nose.y - 60, 15, 15);
    push();
    fill(255);
    rect(pose.nose.x - 93 , pose.nose.y - 140, 70, 210);
    rect(pose.nose.x - 93 , pose.nose.y + 10, 195, 50);
    rotate(15);
    fill(255,0,0);
    rect(pose.nose.x + 76 , pose.nose.y - 210, 80, 200);
    pop(); 
  
    
    // animation 
    
    
    
    
    
    
    
    //guidecross
  
   push();
   if (l > 0){
    l = l -0.5;
   }
   stroke(220, 220, 220, l);
   strokeWeight(5);
   line(320, 0, 320, 480);
   line(0, 240, 680, 240);
   pop();
    
    
  }
    
    
    
    
  }
  
  
 
}




 

    
  
  


function modelLoaded() {
  console.log("poseNet ready");
}
