/*
 Final project face art ai by Aaron Ryder aryde002
  
*/

let video,
    poseNet,
    pose,
    skeleton,
    button,
    iCross = 180,
    lCross = 180,
    jCross = 180,
    kCross = 180,
    xNose = 0,
    yNose = 0,
    xLeye = 0,
    yLeye = 0,
    img,
    scene;


     //animation variables for face art three
     var xz;
     var yz;
     var th;
     var waves;
     var cut;
     var fx;
     var anglex;
     var thing1;
     var thing2;
     var HEIGHTx = 600;
     var WIDTHx = 600;



function preload() {

    //star image png 
    img = loadImage('assets/astar.png');


}



function setup() {
    createCanvas(640, 480);

    //load camera
    video = createCapture(VIDEO, cameraLoaded);
    video.size(640, 480);
    video.hide(); //hide the dom element
    angleMode(DEGREES); //set angles to degrees 
    scene = 1; //set scene to one

    //button link to website 
    button = createButton('Save Image');
    button.position(270, 490);
    button.mousePressed(saveImagexyz);

    //set up animation for face art three 
    xz = WIDTHx / 2;
    yz = HEIGHTx / 2;
    th = 8;
    waves = 105;
    cut = 0;
    fx = 0;
    anglex = PI / 4;
    thing1 = -anglex / 2;
    thing2 = anglex / 2;


}

function saveImagexyz() {
    //save image as png
    save("myImage.png");

}

function cameraLoaded(stream) {
    //load posenet
    poseNet = ml5.poseNet(video, modelLoaded);
    poseNet.on("pose", gotPoses); //setup callback for pose detection

}


//this is called when we can detect a pose
function gotPoses(poses) {
    if (poses.length > 0) {
        pose = poses[0].pose;
        skeleton = poses[0].skeleton;

        //this is calculate distance from camera 
        let nosX = poses[0].pose.keypoints[0].position.x;
        let nosY = poses[0].pose.keypoints[0].position.y;
        let eylX = poses[0].pose.keypoints[1].position.x;
        let eylY = poses[0].pose.keypoints[1].position.y;

        xNose = lerp(xNose, nosX, 0.5);
        xNose = lerp(yNose, nosY, 0.5);
        xLeye = lerp(xLeye, eylX, 0.5);
        yLeye = lerp(yLeye, eylY, 0.5);
       

    }
}

function draw() {
    translate(video.width, 0);
    scale(-1, 1); //this flips the video so it's easier for us
    image(video, 0, 0, video.width, video.height);
    //distance from webcam 
    let d = dist(xNose, yNose, xLeye, yLeye);
    

    //if we can detect a pose, let's draw it
    if (pose) {

        //skeleton 
        for (let bone of skeleton) {
            let a = bone[0];
            let b = bone[1];
            strokeWeight(2);
            stroke(0);
            line(a.position.x, a.position.y, b.position.x, b.position.y);
        }


        // xxxxxxxxxxxxxxxxxxxxxxxxxx SCENE ONE xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        if (scene == 1) {
            
            //instructions 
            push();
            fill(200);
            rect(0, 0, 640, 480);
            fill(0, 0, 0);
            textSize(60)
            text('IA TЯA ƎƆAᖷ', 150, 80);
            textSize(30);
            text('Ɉƨil ИoiɈɔυɿɈƨИI', 220, 120);
            textSize(20)
            text(' TЯA ƎƆAᖷ ƧƧƎƆƆA OT ЯƎꓷЯO ЯƎꓭMUИ ИI მ OT ᛚ ƧƧƎЯᕋ', 40, 160);
            text(' MAƆꓭƎW ƎHT MOЯᖷ YAWA TƎƎᖷ Ԑ OT 𐑕 YATƧ', 100, 200);
            text('ꓷAƎHƧƧOЯƆ ƎHT ᖷO ƎЯTИƎƆ ƎHT ИO ƎƧOИ ƎƆA⅃ᕋ ', 60, 240);
            text('WƎIV ƎMAЯᖷ ᖷO ƎЯTИƎƆ ИIHTIW YꓷOꓭ ᕋ ƎƎꓘ ', 90, 280);
            text(' ИWOꓷ TAƧ Ǝ⅃IHW ꓷƎƧU TƧƎꓭ', 160, 320);
            text('AƎЯA TI⅃ ⅃⅃ƎW ИI ƎƧU ', 200, 360);
            text(' ᕋOЯꓷꓘƆAꓭ ƎЯUƧOᕋXƎ THϱI⅃ ƎƧИƎTИI ꓷIOVA  ', 80, 400);
            text(' ƧƎYƎ ꓷИA ꓷAƎH ƎƆAᖷ ӘИIЯƎVOƆ ꓷIOVA', 100, 440);
            pop();



            //next scene    
            if (keyIsPressed) {
                if (key == 1) {
                    //do something
                    scene = 2;

                }
            }


        }




        // xxxxxxxxxxxxxxxxxxxxxxxxxx SCENE TWO xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


        if (scene == 2) {

            //scene tracking number
            push();
            fill(0);
            textSize(40);
            text("ᛚ", 40, 40);
            pop();


            //face art one 
            fill(210);
            // robot neck
            rect(pose.nose.x - 13, pose.nose.y - 80, 30, 200);
            // robot head
            rect(pose.nose.x - 73, pose.nose.y - 120, 160, 200);
            // robot eye scanning animation 
            fill(0);
            rect(pose.nose.x - 43, pose.nose.y - 70, 100, 40);
            fill(random(10, 255), random(10, 255), random(10, 255));
            rect(pose.nose.x - random(-40, 40), pose.nose.y - 60, 15, 15);
            push();
            fill(255);
            rect(pose.nose.x - 93, pose.nose.y - 140, 70, 210);
            rect(pose.nose.x - 93, pose.nose.y + 10, 195, 50);
            rotate(15);
            fill(255, 0, 0);
            rect(pose.nose.x + 76, pose.nose.y - 210, 80, 200);
            pop();

            
            //guidecross
            push();
            if (iCross > 0) {
                iCross = iCross - 0.6;
            }
            stroke(220, 220, 220, iCross);
            strokeWeight(5);
            line(320, 0, 320, 480);
            line(0, 240, 680, 240);
            pop();


            //next scene    
            if (keyIsPressed) {
                if (key == 2) {
                    //do something
                    scene = 3;

                }
            }
        }


        
        // xxxxxxxxxxxxxxxxxxxxxxxxxx SCENE THREE xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 





        if (scene == 3) {
           
            //scene tracking number
            push();
            fill(0);
            textSize(40);
            text("𐑕", 40, 40);
            pop();



            //face art two 
            push();
            image(img, pose.leftEye.x - 12, pose.leftEye.y - 12, 20, 20);
            image(img, pose.rightEye.x - 12, pose.rightEye.y - 12, 20, 20);
            noFill();

            fill(255, 255, 0);
            stroke(0, 0, 0);
            strokeWeight(5);
            //head 
            rect(pose.nose.x - 0, pose.nose.y - 60, 10, 60);

            fill(255, 255, 0);
            stroke(0, 0, 0);
            strokeWeight(20);

            //mask
            rect(pose.nose.x - 80, pose.nose.y - 120, 170, 60);
            fill(0, random(200, 150));
            rect(pose.nose.x - 90, pose.nose.y - 60, 190, 60);
            fill(random(20, 245), random(20, 245), random(20, 245));
            rect(pose.nose.x - 100, pose.nose.y + 30, 220, 80);
            triangle(pose.nose.x + 120, pose.nose.y, pose.nose.x, pose.nose.y + 120, pose.nose.x - 90, pose.nose.y);
            pop();


            
            //guidecross
            push();
            if (lCross > 0) {
                lCross = lCross - 0.6;
            }
            stroke(220, 220, 220, lCross);
            strokeWeight(5);
            line(320, 0, 320, 480);
            line(0, 240, 680, 240);
            pop();



            // next scene    
            if (keyIsPressed) {
                if (key == 3) {
                    //do something
                    scene = 4;

                }
            }


        }



        // xxxxxxxxxxxxxxxxxxxxxxxxxx SCENE FOUR xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        if (scene == 4) {

            //scene tracking number
            push();
            fill(0);
            textSize(40);
            text("Ԑ", 40, 40);
            pop();


            //animation  
            push();
            stroke(250);
            ellipse(xz, yz, 0, 0);

            colorMode(HSB);

            for (i = cut; i < waves; i++) {

                offset = map(i, cut, waves, -1, 0);
                stroke(map(i, cut, waves, 0, 255), map(i, cut, waves, 0, 255), 127);
                fill(0, 0, 0, 0);
                strokeWeight(th);
                arc(xz, yz, th * i, th * i, thing1 + i * fx * offset / 8, thing2 + i * fx * offset / 8);

            }

            
            //speed of animation 
            fx += 0.5;
            pop();

            
            //face art three
            push();
            fill(240);
            rect(pose.nose.x - 145, pose.nose.y - 150, d - 20);
            noFill();
            stroke(0)
            strokeWeight(5);
            rotate(45);
            rect(pose.nose.x - 80, pose.nose.y - 440, 290, 80);
            rect(pose.nose.x - 80, pose.nose.y - 340, 290, 80);
            rect(pose.nose.x - 80, pose.nose.y - 240, 290, 80);
            pop();
            push();
            fill(0)
            rect(pose.leftEye.x + 90, pose.leftEye.y + 50, d - 400);
            rect(pose.rightEye.x - 10, pose.rightEye.y + 50, d - 400);
            fill(0);
            pop();

 
            //guidecross
            push();
            if (jCross > 0) {
                jCross = jCross - 0.6;
            }
            stroke(220, 220, 220, jCross);
            strokeWeight(5);
            line(320, 0, 320, 480);
            line(0, 240, 680, 240);
            pop();



            // next scene    
            if (keyIsPressed) {
                if (key == 4) {
                    //do something
                    scene = 5;

                }
            }


        }




        // xxxxxxxxxxxxxxxxxxxxxxxxx SCENE FIVE xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


        if (scene == 5) {

            //scene tracking number
            push();
            fill(0);
            textSize(40);
            text("𐊀", 40, 40);
            pop();

            //animation 
            push();
            translate(420, 67);
            noFill();
            strokeWeight(6);

            for (var n = 0; n < 8; n++) {
                stroke(255, 255, 0);
                beginShape()
                for (var ix = 0; ix < 360; ix += 10) {
                    var rad = map(sin(ix * 3 + frameCount), -1, 1, 5, 60)
                    var xx = rad * cos(ix)
                    var yx = rad * sin(ix)
                    vertex(xx, yx)
                }
                endShape(CLOSE)
                rotate(frameCount / 10)
            }
            pop();

            push();
            translate(230, 67);
            noFill();
            strokeWeight(6);

            for (var nz = 0; nz < 8; nz++) {
                stroke(255, 255, 0);
                beginShape()
                for (var ixz = 0; ixz < 360; ixz += 10) {
                    var radz = map(sin(ixz * 3 + frameCount), -1, 1, 5, 60)
                    var xxz = radz * cos(ixz)
                    var yxz = radz * sin(ixz)
                    vertex(xxz, yxz)
                }
                endShape(CLOSE)
                rotate(frameCount / 10)
            }
            pop();



            //face art four 
            stroke(0);
            strokeWeight(10)
            fill(139, 69, 19);
            ellipse(pose.nose.x, pose.nose.y, 200, 260);

            push()
            rotate(10);
            fill(255, 0, 0);
            rect(pose.nose.x - 45, pose.nose.y - 50, 60, 20);
            pop();
            push(0);
            rotate(-10);
            fill(255, 0, 0);
            rect(pose.nose.x - 30, pose.nose.y + 60, 60, 20);
            pop();
            push();
            fill(255, 255, 0);
            rect(pose.nose.x - 10, pose.nose.y - 115, 20, 50);
            pop();
            stroke(255, 0, 0);
            fill(0);
            ellipse(pose.nose.x, pose.nose.y + 80, 50, 50);
            push();
            rotate(-20);
            stroke(255, 255, 0);
            fill(0);
            ellipse(pose.leftEye.x - 80, pose.leftEye.y + 100, 60, 30);
            pop();
            push();
            rotate(20);
            stroke(255, 255, 0)
            fill(0);
            ellipse(pose.rightEye.x + 30, pose.rightEye.y - 115, 60, 30);
            pop();
            fill(0);


            //guidecross
            push();
            if (kCross > 0) {
                kCross = kCross - 0.6;
            }
            stroke(220, 220, 220, kCross);
            strokeWeight(5);
            line(320, 0, 320, 480);
            line(0, 240, 680, 240);
            pop();


            //next scene    
            if (keyIsPressed) {
                if (key == 5) {
                    //do something
                    scene = 6;

                }
            }


        }



        // xxxxxxxxxxxxxxxxxxxxxxxxx SCENE SIX xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


        if (scene == 6) {

            //end scene  
            push();
            fill(200);
            rect(0, 0, 640, 480);
            fill(0, 0, 0);
            textSize(60)
            text('ꓷИƎ', 250, 80);
            text('ɈɿɒɈƨǝɿ oɈ მ ƨƨǝɿq', 80, 180);
            pop();


            //scene tracking number
            push();
            fill(0);
            textSize(40);
            text("ट", 40, 40);
            pop();


            //next scene    
            if (keyIsPressed) {
                if (key == 6) {
                    //do something
                    scene = 1;

                }
            }


        }


    }


}



function modelLoaded() {
    console.log("poseNet ready");
}
